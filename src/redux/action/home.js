import axios from "axios"
import { API_HOST } from "../../config"

export const getFoodData = () => (dispatch) => {
    axios.get(`${API_HOST.url}/api/food`)
    .then(res => {
        // console.log('res food: ', res.data.data.data)
        dispatch({type: 'SET_FOOD', value: res.data.data.data})
    })
    .catch(err => {
        //console.log('error :', err)
    })
}

export const getFoodDataByTypes = (types) => (dispatch) => {
    axios.get(`${API_HOST.url}/api/food?types=${types}`)
    .then((res) => {
        //console.log('res food: ', res.data.data.data)
        if(types === 'new_items'){
            dispatch({type: 'SET_NEW_ITEMS', value: res.data.data.data})
        }
        if(types === 'popular'){
            dispatch({type: 'SET_POPULAR', value: res.data.data.data})
        }
        if(types === 'recommended'){
            dispatch({type: 'SET_RECOMMENDED', value: res.data.data.data})
        }
    })
    .catch(err => {
        //console.log('error :', err.data.data.data)
    })
}