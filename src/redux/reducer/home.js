const initHome = {
    food: [],
    newItems: [],
    popular: [],
    recommended: [],
}

export const homeReducer = (state=initHome, action) => {
    if(action.type === 'SET_FOOD') {
        return {
            ...state,
            food: action.value
        }
    }
    if(action.type === 'SET_NEW_ITEMS') {
        return {
            ...state,
            newItems: action.value
        }
    }
    if(action.type === 'SET_POPULAR') {
        return {
            ...state,
            popular: action.value
        }
    }
    if(action.type === 'SET_RECOMMENDED') {
        return {
            ...state,
            recommended: action.value
        }
    }
    return state;
}