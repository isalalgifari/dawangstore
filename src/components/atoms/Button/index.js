import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const Button = ({text, color = '#6FCF97', onPress, textColor = 'white'}) => {
    return (
        <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <View style={styles.button(color)}>
                <Text style={styles.text(textColor)}>{text}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    button: (color) => ({
        backgroundColor : color,
        borderRadius: 8,
        paddingVertical: 8
    }),
    text: (textColor) => ({
        color: textColor,
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        textAlign: 'center'
    })
})
