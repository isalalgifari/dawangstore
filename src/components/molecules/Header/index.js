import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { IcBack } from '../../../assets/Icon'

const Header = ({title, onBack}) => {
    return (
        <View style={styles.header}>
            {
                onBack && (
                    <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
                        <View style={styles.back}>
                            <IcBack />
                        </View>
                    </TouchableOpacity>
                )
            }
            <View>
                <Text style={styles.text1}>{title}</Text>
            </View>
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    header: {
        backgroundColor: 'white',
        paddingVertical: 24,
        paddingLeft: 24,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text1: {
        color: '#020202',
        fontSize: 22,
        fontFamily: 'Poppins-Medium'
    },
    back: {
        marginRight: 24
    }
})
