import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { IcNext } from '../../../assets'

const ItemListMenuProfile = ({name, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.container}>
                <Text style={styles.name}>{name}</Text>
                <IcNext/>
            </View>
        </TouchableOpacity>
    )
}

export default ItemListMenuProfile

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 7,
    },
    name: {
        fontSize: 14,
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    }
})
