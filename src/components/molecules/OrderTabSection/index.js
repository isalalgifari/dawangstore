import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Dimensions, ScrollView, StyleSheet, Text, View,RefreshControl } from 'react-native';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import { useDispatch, useSelector } from 'react-redux';
import { ItemListFood } from '..';
import { getInProgress, getPastOrders } from '../../../redux/action';

const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicator}
      style={{ 
        backgroundColor: 'white', 
        elevation: 0, 
        shadowOpacity: 0,
        borderBottomColor: '#F2F2F2',
        borderBottomWidth: 1  
      }}
      tabStyle={{ width: 'auto'}}
      renderLabel={({ route, focused, color }) => (
        <Text style={{ 
            fontFamily: 'Poppins-Medium',
            color: focused ? '#020202' : '#8D92A3' 
        }}>
          {route.title}
        </Text>
      )}
    />
  );

const InProgress = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {inProgress} = useSelector((state) => state.orderReducer)
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    dispatch(getInProgress())
  }, [])

  const onRefresh = () => {
    setRefreshing(true);
    dispatch(getInProgress());
    setRefreshing(false);
  };

    return (
      <ScrollView
        refreshControl={
          <RefreshControl 
            refreshing={refreshing} 
            onRefresh={onRefresh}
        />
      }
    >
      <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
        {inProgress.map((order) => {
          return(
            <ItemListFood 
            key={order.id}
            type="in-progress"
            onPress={() => navigation.navigate('OrderDetail', order)} 
            image={{uri:  order.food.picturePath }} 
            nameProduct={order.food.name} 
            price={order.total}
            items={order.quantity} 
          />
          )
        })}
      </View>
    </ScrollView>
    )
  };
   
  const PastOrder = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const {pastOrders} = useSelector((state) => state.orderReducer)
    const [refreshing, setRefreshing] = useState(false);
    
    useEffect(() => {
      dispatch(getPastOrders())
    }, [])

    const onRefresh = () => {
      setRefreshing(true);
      dispatch(getInProgress());
      setRefreshing(false);
    };

      return (
        <ScrollView
          refreshControl={
            <RefreshControl 
              refreshing={refreshing} 
              onRefresh={onRefresh}
          />
        }
      >
          <View style={{ paddingTop: 5, paddingHorizontal: 24 }}>
            {pastOrders.map(order =>{
              return(
                <ItemListFood 
                  type="past-orders"
                  key={order.id}
                  onPress={() => navigation.navigate('OrderDetail', order)} 
                  image={{uri:  order.food.picturePath }} 
                  nameProduct={order.food.name} 
                  price={order.total}
                  items={order.quantity} 
                  date={order.created_at}
                  status={order.status}
                />
              )
            })}
          </View>
        </ScrollView>
    )
  };
   
  const initialLayout = { width: Dimensions.get('window').width };

const OrderTabSection = () => {
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: 'satu', title: 'In Progress' },
        { key: 'dua', title: 'Past Order' },
    ]);
    
    const renderScene = SceneMap({
        satu: InProgress,
        dua: PastOrder,
    });
    return (
        <TabView
            renderTabBar={renderTabBar}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={{ backgroundColor: 'white' }}
        />
    )
}

export default OrderTabSection

const styles = StyleSheet.create({
    indicator: { 
        backgroundColor: '#020202',
        width: '15%',
        marginLeft: '3%'
    }
})
