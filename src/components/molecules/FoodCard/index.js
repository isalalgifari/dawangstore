import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Rating from '../Rating'

const FoodCard = ({image, item, rating, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress} activeOpacity={0.7}>
            <View style={styles.container}>
                <Image source={image} style={styles.img}/>
                <View style={styles.content}>
                    <Text style={styles.text}>{item}</Text>
                    <Rating number={rating} />
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default FoodCard

const styles = StyleSheet.create({
    container: {
        width: 200,
        backgroundColor: 'white',
        borderRadius: 8,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.5,
        shadowRadius: 10,
        elevation: 14,
        borderRadius: 8,
        overflow: 'hidden',
        marginVertical: 15,
        marginRight: 15
    },
    img: {
        width: 200,
        height: 200,
        resizeMode: 'cover'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    },
    content: {
        padding: 12
    },
})
