import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { IcMin, IcPlus } from '../../../assets'

const Counter = ({onValueChange}) => {

    const [value, setValue] = useState(1)
    useEffect(() => {
      onValueChange(value)
    }, [])
    const onCount = (type) => {
        let result = value
        if(type === 'plus'){
            result = value + 1
        }
        if(type === 'minus'){
            if(value > 1){
                result = value - 1
            }
        }
        setValue(result)
        onValueChange(result)
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => onCount('minus')}>
                <IcMin />
            </TouchableOpacity>
            <Text style={styles.number}>{value}</Text>
            <TouchableOpacity onPress={() => onCount('plus')}>
                <IcPlus />
            </TouchableOpacity>
        </View>
    )
}

export default Counter

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    number: {
        marginHorizontal: 10,
        fontSize: 16,
        fontFamily: 'Poppins-Regular'
    }
})
