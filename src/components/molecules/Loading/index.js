import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'

const Loading = () => {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color="#6FCF97" />
            <Text style={styles.text}>Loading Bro ...</Text>
        </View>
    )
}

export default Loading

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0 , 0 , 0 , 0.1)',
        position: 'absolute',
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontFamily: 'Poppins-Regular',
        fontSize: 15,
        marginTop: 10
    }
})
