import ProfileDummy from './pic.png';
import Ip12 from './iphone_12.jpg';
import Ip12Pro from './iphone_12_pro.jpg';
import Ip11Pro from './iphone_11_pro.jpg';
import Ip11 from './iphone_11.jpg'
import IpX from './iphone-x.jpg';
import IpXr from './iphone-xr.jpg';
import IpXsMax from './iphone-xs-max.jpg';
import IpXs from './iphone-xs.jpg';
import Ip8Plus from './iphone-8-plus.jpg';
import Ip8 from './iphone-8.jpg';


export {
    ProfileDummy,
    Ip12,
    Ip12Pro,
    Ip11Pro,
    Ip11,
    IpX,
    IpXr,
    IpXsMax,
    IpXs,
    Ip8Plus,
    Ip8
}