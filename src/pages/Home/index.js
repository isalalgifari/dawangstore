import React, {useEffect} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { StyleSheet, View, ScrollView } from 'react-native'
import { FoodCard, Gap, HomeProfile, HomeTabSection } from '../../components'
import { getFoodData } from '../../redux/action'

const Home = ({navigation}) => {
    const dispatch = useDispatch()
    const {food} = useSelector(state => state.homeReducer)
    useEffect(() => {
        dispatch(getFoodData())
    }, [])
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.page}>
                <HomeProfile />
                <View>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        <View style={styles.cardContainer}>
                            <Gap width={15} />
                            {food.map((items) => {
                                return (
                                    <FoodCard 
                                        key={items.id}
                                        image={{uri: items.picturePath}} 
                                        item={items.name}
                                        rating={items.rate} 
                                        onPress={() => navigation.navigate('FoodDetail', items)} 
                                    />
                                )
                            })}
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.tabContainer}>
                    <HomeTabSection />
                </View>
            </View>
        </ScrollView>
    )
}

export default Home

const styles = StyleSheet.create({
    page: {flex:1},
    cardContainer: {
        flexDirection: 'row',
    },
    tabContainer: {
        flex: 1
    }
})
