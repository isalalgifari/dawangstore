import React, {useState} from 'react'
import { StyleSheet, View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import { Header, TextInput, Gap, Button } from '../../components'
import {useSelector, useDispatch} from 'react-redux'
import { useForm, showMessage } from '../../utils'
import {launchImageLibrary} from 'react-native-image-picker';

const SignUp = ({navigation}) => {
    const[form, setForm] = useForm({
        name: '',
        email: '',
        password: '',
    })

    const [photo, setPhoto] = useState('')

    const dispatch = useDispatch();

    const onSubmit = () => {
        console.log('form: ', form)
        dispatch({type: 'SET_REGISTER', value: form})
        navigation.navigate('SignUpAddress')
    }

    const addPhoto = () => {
        launchImageLibrary({
            quality: 0.5,
            maxHeight: 200,
            maxWidth: 200,     
        }, 
        (response) => {
            console.log('Response = ', response);
            if (response.didCancel || response.error) {
                showMessage('anda tidak memilih foto')
            } else {
                    const source = { uri: response.uri };
                    const dataImage = {
                        uri: response.uri,
                        type: response.type,
                        name: response.fileName
                    }

                    setPhoto(source)
                    dispatch({type: 'SET_PHOTO', value: dataImage})
                    dispatch({type: 'SET_UPLOAD_STATUS', value: true})
                }
          });
    }

    return (
        <ScrollView 
            contentContainerStyle = {{ flexGrow:1 }}
            showsVerticalScrollIndicator={false} 
        >
            <View  style={styles.page}>
                <Header title='Sign Up' onBack={() => navigation.goBack()} />
                <View style={styles.container}>
                    <View style={styles.photo}> 
                        <TouchableOpacity onPress={addPhoto}>
                            <View style={styles.borderPhoto}>
                                {photo ? (
                                    <Image source={photo} style={styles.photoContainer}/>
                                ) : (
                                    <View style={styles.photoContainer}>
                                        <Text style={styles.addPhoto}>Add Photo</Text>
                                    </View>    
                                )}
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Gap height={16} />
                    <TextInput 
                        label='Fullname' 
                        placeholder='Input fullname'
                        value={form.name}
                        onChangeText={(value) => setForm('name', value)} 
                    />
                    <Gap height={16} />
                    <TextInput 
                        label='Email' 
                        placeholder='Input email'
                        value={form.email}
                        onChangeText={(value) => setForm('email', value)}
                    />
                    <Gap height={16} />
                    <TextInput 
                        label='Password' 
                        placeholder='Input password' 
                        value={form.password}
                        onChangeText={(value) => setForm('password', value)}
                        secureTextEntry
                    />
                    <Gap height={24} />
                    <Button 
                        text='Continue'
                        onPress={onSubmit}
                    />
                </View>
            </View>
        </ScrollView>
    )
}

export default SignUp

const styles = StyleSheet.create({
    page: {
        flex:1
    },
    container:{
        paddingHorizontal: 24,
        paddingTop: 24,
        backgroundColor: 'white',
        marginTop: 10,
        flex: 1
    },
    photo: {
        alignItems: 'center'
    },
    borderPhoto: {
        borderWidth: 1, 
        borderColor: '#8D92A3',
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center'
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        justifyContent: 'center',
        alignItems: 'center'
    },
    addPhoto: {
        fontFamily:'Poppins-Light',
        fontSize: 14,
        textAlign: 'center'
    }
})
