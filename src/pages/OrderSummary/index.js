import axios from 'axios'
import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { WebView } from 'react-native-webview'
import { Button, Header, ItemListFood, ItemValue, Loading } from '../../components'
import { API_HOST } from '../../config'
import { getData } from '../../utils'

const OrderSummary = ({navigation, route}) => {
    const {item, transaction, userProfile} = route.params;
    const [isPaymentOpen, setIsPaymentOpen] = useState(false)
    const [paymentURL, setPaymentURL] = useState('https://google.com')

    const onCheckout = () => {
        const data = {
            food_id: item.id,
            user_id: userProfile.id,
            quantity: transaction.totalItem,
            total: transaction.total,
            status: 'PENDING',
        }
        getData('token').then(resToken => {
            axios.post(`${API_HOST.url}/api/checkout`, data, {
                headers: {
                    'Authorization': resToken.value
                }
            })
            .then(res => {
                console.log('suskses checkout :', res.data)
                setIsPaymentOpen(true)
                setPaymentURL(res.data.data.payment_url)
            })
            .catch(err => {
                console.log('error checkout :', err.data)
            })
        })
    }

    const onNavChange = (state) => {
        console.log('nav : ', state)
        const urlSuccess = `${API_HOST.url}/midtrans/success?order_id=23&status_code=201&transaction_status=pending`
        const titleWeb = 'Laravel'
        if(state.title === titleWeb){
            navigation.reset({index: 0, routes: [{name: 'SuccessOrder'}]})
        }

    }

    if(isPaymentOpen){
        return (
            <>
                <Header 
                    title="Payment" 
                    onBack={() => setIsPaymentOpen(false)}
                />
                <WebView
                    source={{ uri: paymentURL }}
                    startInLoadingState={true}
                    renderLoading={() => <Loading />}
                    onNavigationStateChange={onNavChange}
                />
            </>
        );
    }
    return (
        <View>
            <Header 
                title="Payment" 
                onBack={() => navigation.goBack()}
            />
            <View>
                <View style={styles.container}>
                    <Text style={styles.label}>Item Ordered</Text>
                    <ItemListFood 
                        type="order-summary"
                        image={{ uri: item.picturePath }}
                        nameProduct={item.name}
                        price={item.price}
                        items={transaction.totalItem}
                    />
                    <Text style={styles.label}>Detail Transaction</Text>
                    <ItemValue 
                        label={item.name}
                        value={transaction.totalPrice}
                        type='currency'
                    />
                    <ItemValue 
                        label='Driver'
                        value={transaction.driver} 
                        type='currency'
                    />
                    <ItemValue 
                        label='Tax 10%'
                        value={transaction.tax}
                        type='currency'
                    />
                    <ItemValue 
                        label='Total Price'
                        value={transaction.total}
                        type='currency'
                        valueColor={'#1ABC9C'}
                    />
                </View>
            </View>
            <View style={styles.container}>
                <Text style={styles.label}>Deliver to</Text>
                <ItemValue 
                    label='Name'
                    value={userProfile.name}
                />
                <ItemValue 
                    label='Phone No.'
                    value={userProfile.phoneNumber}
                />
                <ItemValue 
                    label='Address'
                    value={userProfile.address}
                />
                <ItemValue 
                    label='House No.'
                    value={userProfile.houseNumber}
                />
                <ItemValue 
                    label='City'
                    value={userProfile.city}
                />
            </View>
            <View style={styles.button}>
                <Button 
                    text="Chekout Now" 
                    onPress={onCheckout}
                />
            </View>
        </View>
    )
}

export default OrderSummary

const styles = StyleSheet.create({
    container: {
        marginTop: 24,
        paddingHorizontal: 15,
        paddingVertical: 5,
        backgroundColor: 'white'
    },
    label: {
        fontFamily: 'Poppins-Regular',
        fontSize: 14,
        color: '#020202',
        marginBottom: 5
    },
    button: {
        marginVertical: 24,
        paddingHorizontal: 24
    }
})
