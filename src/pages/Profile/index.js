import React, { useEffect, useState } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { ProfileTabSection } from '../../components'
import { getData, storeData, showMessage } from '../../utils'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {launchImageLibrary} from 'react-native-image-picker'
import axios from 'axios'
import { API_HOST } from '../../config'

const Profile = () => {
    const [userProfile, setUserProfile] = useState({})
    useEffect(() => {
        updateUserProfile()
    }, [])

    const updateUserProfile = () => {
        getData('userProfile').then(res => {
            setUserProfile(res)
        })
    }

    const updatePhoto = () => {
        launchImageLibrary({
            quality: 0.5,
            maxHeight: 200,
            maxWidth: 200,     
        }, (response) => {
            //console.log('Response = ', response);
            if (response.didCancel || response.error) {
                showMessage('anda tidak memilih foto')
            } else {
                    const source = { uri: response.uri };
                    const dataImage = {
                        uri: response.uri,
                        type: response.type,
                        name: response.fileName
                    }

                    const photoForUpload = new FormData()
                    photoForUpload.append('file', dataImage)
                    getData('token').then(resToken => {
                        axios.post(`${API_HOST.url}/api/user/photo`, photoForUpload, {
                            headers: {
                                Authorization : resToken.value,
                                'Content-Type' : 'multipart/form-data'
                            }
                        })
                        .then(res => {
                            getData('userProfile').then(resUser => {
                                showMessage('update foto berhasil', 'success')
                                resUser.profile_photo_url = `${API_HOST.url}/storage/${res.data.data[0]}`
                                storeData('userProfile', resUser).then(() => {
                                    updateUserProfile()
                                })
                            })
                        }).catch(err => {
                            console.log('err: ', err.response)
                        })
                    })

                }
          });
    }

    return (
        <View style={styles.page}> 
            <View style={styles.page}>
                <View style={styles.profileDetail}>
                    <View style={styles.photo}>
                        <TouchableOpacity onPress={updatePhoto}>
                            <View style={styles.borderPhoto}>
                                <Image style={styles.photoContainer} source={{ uri: userProfile.profile_photo_url }} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.name}>{userProfile.name}</Text>
                    <Text style={styles.email}>{userProfile.email}</Text>
                </View>
                <View style={styles.content}>
                    <ProfileTabSection />
                </View>
            </View>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    profileDetail: {
        backgroundColor: 'white',
        paddingVertical: 26
    },
    photo: {
        alignItems: 'center',
        marginBottom: 16
    },
    borderPhoto: {
        borderWidth: 1,
        borderColor: '#8D92A3',
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center',
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        padding: 24,
    },
    name: {
        fontFamily: 'Poppins-Medium',
        fontSize: 18,
        color: "#020202",
        textAlign: 'center'
    },
    email: {
        fontFamily: 'Poppins-Light',
        fontSize: 14,
        color: '#8D92A3',
        textAlign: 'center'
    },
    content: {
        flex: 1,
        marginTop: 24,
    }
})
